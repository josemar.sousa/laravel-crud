<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

//Login
Route::get('/login', ['as' => 'site.login', 'uses' => 'Site\LoginController@index']);
Route::post('/login/entrar', ['as' => 'site.login.entrar', 'uses' => 'Site\LoginController@entrar']);
Route::get('/login/sair', ['as' => 'site.login.sair', 'uses' => 'Site\LoginController@logout']);

//Home
Route::get('/', ['as' => 'site.home', 'uses' => 'Site\HomeController@index']);

Route::group(['middleware' => 'auth'], function () {
    //Cursos
    Route::get('/admin/cursos', ['as' => 'admin.cursos', 'uses' => 'admin\CursoController@index']);
    Route::get('/admin/cursos/add', ['as' => 'admin.cursos.adcionar', 'uses' => 'admin\CursoController@adicionar']);
    Route::post('/admin/cursos/salvar', ['as' => 'admin.cursos.salvar', 'uses' => 'admin\CursoController@salvar']);
    Route::get('/admin/cursos/editar/{id}', ['as' => 'admin.cursos.editar', 'uses' => 'admin\CursoController@editar']);
    Route::put('/admin/cursos/atualizar/{id}', ['as' => 'admin.cursos.atualizar', 'uses' => 'admin\CursoController@atualizar']);
    Route::get('/admin/cursos/deletar/{id}', ['as' => 'admin.cursos.deletar', 'uses' => 'admin\CursoController@deletar']);
});

//Contato - Testes
Route::get('/contato/{id?}', ['uses' => 'ContatoController@index']);
Route::post('/contato/{id?}', ['uses' => 'ContatoController@criar']);
Route::put('/contato/{id?}', ['uses' => 'ContatoController@editar']);

/*Route::get('/contato/{id?}', function () {
    return view('contato');
});

Route::post('/contato', function () {
    dd($_POST);
    return 'contato POST';
});

Route::put('/contato', function () {
    return 'contato PUT';
});*/
