@extends('layout.site')

@section('titulo', 'Cursos')

@section('conteudo')

<div class="container">
    <h3 class="center">Lista de cursos</h3>
    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Imagem</th>
                    <th>Publicado</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($registros as $reg)
                <tr>
                    <td>{{ $reg->id }}</td>
                    <td>{{ $reg->titulo }}</td>
                    <td>{{ $reg->descricao }}</td>
                    <td><img width="70" height="30" src="{{ asset($reg->imagem) }}" alt="{{ $reg->titulo }}"></td>
                    <td>{{ $reg->publicado }}</td>
                    <td>
                        <a class="btn deep-orange" href="{{ route('admin.cursos.editar',$reg->id) }}">Editar</a>
                        <a class="btn red" href="{{ route('admin.cursos.deletar',$reg->id) }}">Deletar</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <a class="btn blue" href="{{ route('admin.cursos.adcionar') }}">Adicionar</a>
    </div>
</div>



@endsection()