<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Curso;

class HomeController extends Controller
{
    public function index()
    {
        //$cursos = Curso::all();
        $cursos = Curso::paginate(3); //Exibir todos os cursos com paginação
        return view('home', compact('cursos'));
    }
}
