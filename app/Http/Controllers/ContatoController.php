<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Contato;

class ContatoController extends Controller
{
    public function index()
    {
        $contatos = [
            (object) ["nome" =>"Mazinho", "tel"=>"(71)99261-2552"],
            (object) ["nome" =>"Shirley", "tel"=>"(71)99261-2555"]
        ];

        $contato = new Contato();
        $con = $contato->lista();
        dd($con);

        return view('contato.index', compact('contatos'));
    }

    public function criar(Request $req)
    {
        dd($req->all()) ;
        return "Esse é o metodo criar do ContatoController";
    }

    public function editar(Request $req)
    {
        dd($req->all()) ;
        return "Esse é o metodo editar do ContatoController";
    }
}
