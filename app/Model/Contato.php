<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    public function lista()
    {
        return (object)[
            'nome' => 'Mazinho',
            'tel' => '(71)99261-2552'
        ];
    }
}
